#!/usr/bin/python3

import sys
import os.path
import argparse
from sys import argv


class Cmd:

    def prompt():
        parser = argparse.ArgumentParser(description='Align DNA/RNA sequences')
        parser.add_argument('-query', '-q' , help='query sequence in fasta', required=True )
        parser.add_argument('-subject', '-s' ,help='target sequence in fasta', required=True )
        parser.add_argument('-gap_open', '-go' ,type=int ,help='open penality, default=-10')
        parser.add_argument('-gap_extend', '-ge' ,type=int ,help='extend penalty, default=-1')
        parser.add_argument('-transition', '-ts' ,type=int ,help='transition penality, default=1')
        parser.add_argument('-transversion', '-tv' ,type=int ,help='transversion penality, default=-1')
        parser.add_argument('-match', '-m' ,type=int ,help='match penality, default=2')
        args = parser.parse_args()
        return [args.query,args.subject], {'MATCH': args.match ,'GAP':  args.gap_open, 'GAPEXTENSION': args.gap_extend, 'TRANSITION': args.transition,'TRANSVERSION': args.transversion}
