#!/usr/bin/python3

import numpy as np

class Align:

    #This function returns to values for cases of match or mismatch
    @staticmethod
    def Diagonal(n1,n2,pt):
        purine=['a','g']
        pyrimidine=['c','t']
        if(n1 == n2):
            return pt['MATCH']
        elif(((n1 in purine) & (n2 in purine)) | ((n1 in pyrimidine) & (n2 in pyrimidine))):
            return pt['TRANSITION']
        # elif((n1 in purine & n2 in pyrimidine) | (n1 in pyrimidine & n2 in purine))
        #     return pt['TRANSITION']
        else:
            return pt['TRANSVERSION']

    #This function gets the optional elements of the aligment matrix and returns the elements for the pointers matrix.
    @staticmethod
    def Pointers(di,ho,ve):
        pointer = max(di,ho,ve) #based on python default maximum(return the first element).
        if(di == pointer):
            return 'D'
        elif(ho == pointer):
            return 'H'
        else:
            return 'V'

    def NW(custom_penalty,s1,s2,match = 2, gap = -10, gapextension= -1, transition = 1, transversion = -1):
        penalty = {'MATCH': match, 'GAP': gap, 'GAPEXTENSION': gapextension, 'TRANSITION': transition, 'TRANSVERSION': transversion} #A dictionary for all the penalty values.
        for key,value in custom_penalty.items():
            if value!=None:
                penalty[key]=value

        n = len(s1) + 1 #The dimension of the matrix columns.
        m = len(s2) + 1 #The dimension of the matrix rows.
        al_mat = np.zeros((m,n),dtype = int) #Initializes the alighment matrix with zeros.
        p_mat = np.zeros((m,n),dtype = str) #Initializes the alighment matrix with zeros.

        #Scans all the first rows element in the matrix and fill it with "gap penalty"
        for i in range(m):
            al_mat[i][0] = penalty['GAP'] + penalty['GAPEXTENSION'] * (i-1)
            p_mat[i][0] = 'V'

        #Scans all the first columns element in the matrix and fill it with "gap penalty"
        for j in range (n):
            al_mat[0][j] = penalty['GAP'] + penalty['GAPEXTENSION'] * (j-1)
            p_mat [0][j] = 'H'

        #Fill the matrix with the correct values.
        al_mat[0][0]= 0
        p_mat [0][0] = 0 #Return the first element of the pointer matrix back to 0.
        for i in range(1,m):
            for j in range(1,n):
                di = al_mat[i-1][j-1] + Align.Diagonal(s1[j-1],s2[i-1],penalty) #The value for match/mismatch -  diagonal.
                ho = al_mat[i][j-1] + penalty['GAPEXTENSION'] if al_mat[i][j-1] == al_mat[i][j-2] + penalty['GAP'] else al_mat[i][j-1] + penalty['GAP']#The value for gap - horizontal.(from the left cell)
                ve = al_mat[i-1][j] + penalty['GAPEXTENSION'] if al_mat[i-1][j] == al_mat[i-2][j] + penalty['GAP'] else al_mat[i-1][j] + penalty['GAP']#The value for gap - vertical.(from the upper cell)
                al_mat[i][j] = max(di,ho,ve) #Fill the matrix with the maximal value.(based on the python default maximum)

                p_mat[i][j] = Align.Pointers(di,ho,ve)
        return al_mat,p_mat
