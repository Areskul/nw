#!/usr/bin/python3

from io import StringIO

class Printer:

    def slice_per(source):
        return [source[i:i+30] for i in range(0,len(source),30)]

    def traceback(s1,s2,p_mat,al_mat):
        i , j = len(s2), len(s1)
        symb= list()
        ns1=list()
        ns2=list()
        gap=0
        match=0
        mismatch=0
        purine=['a','g']
        pyrimidine=['c','t']

        while i>0 or j>0 :
            if p_mat[i][j]=='D':
                if s1[j-1]==s2[i-1]:
                    ns1.append(s1[j-1])
                    ns2.append(s2[i-1])
                    symb.append('|')
                    match+=1
                else:
                    ns1.append(s1[j-1])
                    ns2.append(s2[i-1])
                    symb.append(':')
                    mismatch+=1
                i-=1
                j-=1
            if p_mat[i][j]=='H':
                ns1.append(s1[j-1])
                ns2.append('-')
                gap+=1
                symb.append(' ')
                j-=1
            if p_mat[i][j]=='V':
                ns2.append(s2[i-1])
                ns1.append('-')
                symb.append(' ')
                i-=1


        print('Gap = '+ str(gap))
        print('Match = '+ str(match))
        print('Mismatch = '+ str(mismatch))
        symb.reverse()
        ns1.reverse()
        ns2.reverse()

        seq1 = Printer.slice_per(ns1)
        sy = Printer.slice_per(symb)
        seq2 = Printer.slice_per(ns2)


        print('\n')
        for i in range(len(sy)):
            string=''
            print(*seq1[i])
            print(*sy[i])
            print(*seq2[i])
            print('\n')
        print('\n')

    def stat(s1,s2,al_mat,p_mat):
        print('\n')
        print("Matrice de score:")
        print(al_mat)
        print('\n')
        print("Matrice de traceback:")
        print(p_mat)
        print(" D=DIAGONAL H=HORIZONTAL V=VERTICAL")
        print('\n')
        score=al_mat[len(s2)][len(s1)]
        print('Score max = ' + str(score))
