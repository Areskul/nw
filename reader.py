#!/usr/bin/python3

class Reader:

    @staticmethod
    def read(file):
        seq=''
        id=''
        dic={}
        for line in file:
            if line.startswith('>'):
                id = line[1:]
            else:
                seq += line.strip().upper()
            dic[id]=seq
        return dic

    def fasta(filename):
        file = open(filename,"r")
        seqDic = Reader.read(file)
        return seqDic
